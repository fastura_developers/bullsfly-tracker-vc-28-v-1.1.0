import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { Device } from '@ionic-native/device';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { ComProvider } from '../../providers/com/com';
import { OneSignal } from '@ionic-native/onesignal';


interface deviceInterface {
  id?: string
};

@IonicPage()
@Component({
  selector: 'page-dtabs',
  templateUrl: 'dtabs.html'
})
export class DtabsPage {

  public items:Array<any>=[];
  public data:any = {};
  public deviceInfo: deviceInterface = {};
  public myDate: any = new Date().toISOString();

  reportRoot = 'ReportPage';
  callsRoot = 'CallsPage';
  levelsRoot = 'LevelsPage';
  newsRoot = 'NewsPage';
  chartsRoot = 'ChartsPage';
  calendarRoot = 'CalendarPage';

  constructor(public navCtrl: NavController, private device: Device, public http: Http, private com: ComProvider,private oneSignal: OneSignal) {
    this.data.id = this.device.uuid;
    this.http = http;
    this.expiredpage();
    this.oneSignalapp();
  }
  expiredpage() {
    var link = 'http://test.fastura.com/bullsfly/Subscribe-data.php';
    var myData = JSON.stringify({id: this.data.id});
    this.http.post(link, myData).map(res => res.json())
    .subscribe((data : any) =>
    {
       console.dir(data);
       console.dir(this.myDate);
       this.items = data;
       if(this.items <= this.myDate){
        this.callsRoot = 'SubscribePage';
        this.levelsRoot = 'SubscribePage';
        this.chartsRoot = 'ChartsPage';
        this.newsRoot = 'NewsPage';
             }
      else{
        this.callsRoot = 'CallsPage';
        this.levelsRoot = 'LevelsPage';
        this.chartsRoot = 'ChartsPage';
        this.newsRoot = 'NewsPage';
            }
    }, error => {
    console.log("Oooops!");
    });
  }
  shareFB(){
    this.com.shareFacebook();
  }
  sharewap(){
    this.com.shareWhatsapp();
  }
  sharetwit(){
    this.com.shareTwitter();
  }
  moreshare(){
    this.com.moreShare();
  }
  oneSignalapp(){
    this.oneSignal.startInit('7a985bcf-3d27-4d04-b546-47eb0e17e184', '790056736225');
  
    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
  
    this.oneSignal.handleNotificationReceived().subscribe(() => {
        // do something when notification is received
      });
  
    this.oneSignal.handleNotificationOpened().subscribe(() => {
      // do something when a notification is opened
      });
  
    this.oneSignal.endInit();
    }  

}
