import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { InAppBrowser,InAppBrowserEvent } from '@ionic-native/in-app-browser';


import { AccountPage } from '../account/account';
import { timer } from 'rxjs/observable/timer';
/**
 * Generated class for the SubscribePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-subscribe',
  templateUrl: 'subscribe.html',
})
export class SubscribePage {

  
  public surl:string="http://test.fastura.com/b/success.php";
  public furl:string="http://test.fastura.com/b/failure.php";

  constructor(public navCtrl: NavController, public navParams: NavParams,public iab:InAppBrowser) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SubscribePage');
  }

  d(){
    const browser=this.iab.create("http://test.fastura.com/b/bmobile.php","_self",{
      location:'no',
      clearcache:'yes',
      hardwareback:'no',
    });
    browser.on('loadstart').subscribe((event:InAppBrowserEvent)=>{
      if(event.url===this.surl){
       timer(3000).subscribe(()=> browser.close())
        this.navCtrl.push(AccountPage);
      }
      else if(event.url===this.furl){
       timer(3000).subscribe(()=> browser.close())
       this.navCtrl.push(AccountPage);
       }
    });
  }
}

