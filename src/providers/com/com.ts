import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Platform, AlertController } from 'ionic-angular';

@Injectable()
export class ComProvider {

  public alertShown:boolean=false;
  showSplash = true;
  public appurl : string ='https://play.google.com/store/apps/details?id=com.fastura.bullsflymcx';
  public img : string ='https://lh3.googleusercontent.com/Hmj5uWkyQgtC-oxe0umoD-0Gf-guvBHVk36suRthqFXSefRm1BJvHVVtcUmVWXl1aP4=s180-rw';

  constructor(public platform: Platform, public http: HttpClient, private socialSharing: SocialSharing, public alertCtrl: AlertController) {
    console.log('Hello ComProvider Provider');
  }
  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Bullsfly Tracker',
      message: 'Do you want Exit?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
            this.alertShown=false;
          }
        },
        {
          text: 'Yes',
          handler: () => {
            console.log('Yes clicked');
            this.platform.exitApp();
          }
        }
      ]
    });
     alert.present().then(()=>{
      this.alertShown=true;
    });
  }

  moreShare(){
    this.socialSharing.share("Bullsfly Tracker","",this.img,this.appurl).then(()=>{
    console.log("success");
    }).catch(()=>{
    console.error("failed");
    });
  }

  shareTwitter(){
    this.socialSharing.shareViaTwitter("Bullsfly Tracker",this.img,this.appurl).then(()=>{
    console.log("success");
    }).catch(()=>{
    console.error("failed");
    });
  }

  shareFacebook(){
    this.socialSharing.shareViaFacebook("Bullsfly Tracker","",this.appurl).then(()=>{
    console.log("success");
    }).catch(()=>{
    console.error("failed");
    });
  }

  shareWhatsapp(){
    this.socialSharing.shareViaWhatsApp("Bullsfly Tracker",this.img,this.appurl).then(()=>{
    console.log("success");
    }).catch(()=>{
    console.error("failed");
    });
  }

}
